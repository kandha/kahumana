package org.kahumana;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

@SuppressWarnings("serial") public class APIServlet extends HttpServlet {
	HttpServletRequest req;
	HttpServletResponse res;
	DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	static Logger logger = Logger.getLogger(APIServlet.class.getName());
	static final String DEFAULTPASS = DigestUtils.sha256Hex("kahumana");

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException {
		try {
			this.req = req;
			this.res = res;
			String op = req.getPathInfo();
			if (op != null) {
				if ("/events".equals(op)) {
					events();
				} else if (op.startsWith("/event/delete")) {
					deleteEvent(Long.parseLong(op.substring(14)));
				} else if (op.startsWith("/export/")) {
					export(op.substring(8));
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			throw new IOException(e);
		}
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws IOException {
		try {
			this.req = req;
			this.res = res;
			res.setContentType("application/json");
			String op = req.getPathInfo();
			if ("/event".equals(op)) {
				event();
			} else if ("/admin".equals(op)) {
				admin();
			} else if ("/login".equals(op)) {
				login();
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			throw new IOException(e);
		}
	}

	private void login() throws Exception {
		String userPass = DEFAULTPASS;
		try {
			Entity adminEntity = datastore.get(KeyFactory.createKey("Admin",
					"admin"));
			userPass = (String) adminEntity.getProperty("userpassword");
			if (userPass == null) {
				userPass = DEFAULTPASS;
			}
		} catch (Exception e) {

		}
		if (userPass
				.equals(DigestUtils.sha256Hex(req.getParameter("password")))) {
			res.getWriter().write(
					new JSONObject().put("success", true).toString());
		} else {
			res.getWriter().write(
					new JSONObject().put("error", "unauthorized").toString());
		}
	}

	private void events() throws Exception {
		//Get List of options, events
		JSONObject resJSON = new JSONObject();
		//Read options from Datastore
		Entity adminEntity = datastore.get(KeyFactory.createKey("Admin",
				"admin"));
		JSONArray clients = new JSONArray(
				(String) adminEntity.getProperty("client"));
		Map<String, Boolean> clientsMap = new HashMap<String, Boolean>();
		for (int i = 0, l = clients.length(); i < l; i++) {
			clientsMap.put(clients.getString(i), true);
		}
		resJSON.put("client", clients);
		resJSON.put("location",
				new JSONArray((String) adminEntity.getProperty("location")));
		resJSON.put("antecedent",
				new JSONArray((String) adminEntity.getProperty("antecedent")));
		resJSON.put("consequence",
				new JSONArray((String) adminEntity.getProperty("consequence")));
		String behavior = (String) adminEntity.getProperty("behavior");
		if (behavior != null) {
			resJSON.put("behavior", new JSONObject(behavior));
		}
		//Read events from Datastore
		JSONArray eventsJSON = new JSONArray();
		resJSON.put("events", eventsJSON);
		Query qe = new Query("Event");
		PreparedQuery pqe = datastore.prepare(qe);
		for (Entity event : pqe.asIterable()) {
			JSONObject eventJSON = toJSON(event);
			eventJSON.put("id", event.getKey().getId());
			//Check if the client is still active
			if (clientsMap.containsKey(eventJSON.getString("client"))) {
				eventsJSON.put(eventJSON);
			}
		}
		res.setContentType("application/json");
		res.getWriter().write(resJSON.toString());
	}

	@SuppressWarnings("unchecked") private void event() throws Exception {
		//Save event
		String id = req.getParameter("id");
		Entity event = null;
		if (id != null && !id.isEmpty()) {
			event = new Entity("Event", Long.parseLong(id));
		} else {
			event = new Entity("Event");
		}
		Map<String, String[]> parameters = req.getParameterMap();
		for (String parameter : parameters.keySet()) {
			event.setProperty(parameter, parameters.get(parameter)[0]);
		}
		datastore.put(event);
		JSONObject resJSON = new JSONObject();
		resJSON.put("id", event.getKey().getId());
		res.getWriter().write(resJSON.toString());
	}

	private void admin() throws Exception {
		String adminPassword = DEFAULTPASS;
		try {
			Entity adminEntity = datastore.get(KeyFactory.createKey("Admin",
					"admin"));
			if (adminEntity != null) {
				adminPassword = (String) adminEntity.getProperty("password");
				if (adminPassword == null) {
					adminPassword = DEFAULTPASS;
				}
			}
		} catch (Exception e) {

		}
		String reqPassword = req.getParameter("password");
		if (reqPassword != null
				&& adminPassword.equals(DigestUtils.sha256Hex(reqPassword))) {
			Entity adminEntity = new Entity("Admin", "admin");
			String newPassword = req.getParameter("newpassword");
			if (newPassword != null) {
				//Update password
				adminEntity.setProperty("password",
						DigestUtils.sha256Hex(newPassword));
			}
			//Update admin options
			String clients = req.getParameter("client");
			adminEntity.setProperty("client", clients);
			adminEntity.setProperty("location", req.getParameter("location"));
			adminEntity.setProperty("antecedent",
					req.getParameter("antecedent"));
			adminEntity.setProperty("consequence",
					req.getParameter("consequence"));
			adminEntity.setProperty("behavior", req.getParameter("behavior"));
			String userpassword = req.getParameter("userpassword");
			if (userpassword != null && userpassword.length() != 0)
				adminEntity.setProperty("userpassword",
						DigestUtils.sha256Hex(userpassword));
			//Save to Datastore
			datastore.put(adminEntity);
			res.getWriter().write("{}");
		} else {
			res.getWriter().write(
					new JSONObject().put("error", "unauthorized").toString());
		}
	}

	private void export(String filter) throws Exception {
		//Export data as CSV
		res.setContentType("text/csv");
		Query qe = new Query("Event");
		boolean all = filter.equals("all");
		Map<String, Boolean> clientsMap = null;
		if (all) {
			Entity adminEntity = datastore.get(KeyFactory.createKey("Admin",
					"admin"));
			JSONArray clients = new JSONArray(
					(String) adminEntity.getProperty("client"));
			clientsMap = new HashMap<String, Boolean>();
			for (int i = 0, l = clients.length(); i < l; i++) {
				clientsMap.put(clients.getString(i), true);
			}
		} else {
			qe.setFilter(new FilterPredicate("client", FilterOperator.EQUAL,
					filter));
		}
		PreparedQuery pqe = datastore.prepare(qe);
		PrintWriter w = res.getWriter();
		w.write("#Date,Time,Client,Behavior,Count,Duration,Duration in Seconds,Location,Antecedent,Consequence,Comments\n");
		for (Entity event : pqe.asIterable()) {
			String behaviors = (String) event.getProperty("behavior");
			List<String> behaviorsList = new ArrayList<String>();
			if (behaviors.startsWith("{") && behaviors.endsWith("}")) {
				try {
					JSONObject valueJSON = new JSONObject(behaviors);
					@SuppressWarnings("unchecked") Iterator<String> values = valueJSON
							.keys();
					while (values.hasNext()) {
						behaviorsList.add(values.next());
					}
				} catch (JSONException je) {

				}
			} else {
				behaviorsList.add(behaviors);
			}
			for (String behavior : behaviorsList) {
				StringBuilder csv = new StringBuilder();
				addStringToCSV(csv, event, "date");
				addStringToCSV(csv, event, "time");
				String client = (String) event.getProperty("client");
				if (all && !clientsMap.containsKey(client)) {
					continue;
				}
				addStringToCSV(csv, event, "client");
				csv.append(escapeCSV(behavior)).append(",");
				addStringToCSV(csv, event, "count", "1");
				addStringToCSV(csv, event, "duration", "00:00:00");
				addStringToCSV(csv, event, "seconds", "0");
				addStringToCSV(csv, event, "location");
				addStringToCSV(csv, event, "antecedent");
				addStringToCSV(csv, event, "consequence");
				addStringToCSV(csv, event, "comments");
				csv.append("\n");
				w.write(csv.toString());
				w.flush();
			}
		}
	}

	private void addStringToCSV(StringBuilder csv, Entity event, String property) {
		addStringToCSV(csv, event, property, null);
	}

	private void addStringToCSV(StringBuilder csv, Entity event,
			String property, String defaultValue) {
		String value = (String) event.getProperty(property);
		if (value == null) {
			value = defaultValue;
		}
		csv.append(escapeCSV(value)).append(",");
	}

	private String escapeCSV(String value) {
		if (value.contains("\"")) {
			value = value.replaceAll("\"", "\"\"");
		}
		if (value.contains("\"") || value.contains("\n")
				|| value.contains("\r") || value.contains(",")) {
			value = "\"" + value + "\"";
		}
		return value;
	}

	private void deleteEvent(long id) {
		datastore.delete(KeyFactory.createKey("Event", id));
	}

	private JSONObject toJSON(Entity entity) throws JSONException {
		JSONObject json = new JSONObject();
		Map<String, Object> properties = entity.getProperties();
		for (String key : properties.keySet()) {
			json.put(key, properties.get(key));
		}
		return json;
	}

}
